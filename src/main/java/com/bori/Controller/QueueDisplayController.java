package com.bori.Controller;

import com.bori.Controller.Util.TaskColor;
import com.bori.Model.Server.Server;
import com.bori.Model.Server.ServerStatistics;
import com.bori.Model.Task.Task;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class QueueDisplayController implements PropertyChangeListener {

    @FXML
    public ScrollPane tasksScrollPane;
    @FXML
    public HBox tasksBox;
    @FXML
    public Label queueNrLabel;
    @FXML
    public Label noExecutedTasksLabel;
    @FXML
    public Label avgWaitingTimeLabel;
    @FXML
    public Label noTasksLabel;

    private Server server;
    private static final DecimalFormat df = new DecimalFormat("###.###");
    private final List<TaskDisplayController> taskDisplayControllers = new ArrayList<>();

    @FXML
    public void initialize() {
        HBox.setHgrow(tasksScrollPane, Priority.ALWAYS);
        tasksBox.setFillHeight(true);
        tasksBox.setAlignment(Pos.CENTER_RIGHT);
        tasksScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        tasksScrollPane.setFitToHeight(true);
    }

    public void initServer(Server server) throws IOException {
        if (this.server != null) {
            throw new IllegalStateException();
        }
        this.server = server;
        queueNrLabel.setText("Queue nr. " + server.getId());
        server.addPropertyChangeListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case Server.TASK_FINISHING_EVENT:
                Platform.runLater(this::removeFirstTask);
                break;
            case Server.STATISTICS_CHANGE_EVENT:
                Platform.runLater(() -> resetStatistics((ServerStatistics) evt.getNewValue()));
                break;
            case Server.NEW_TASK_EVENT:
                Platform.runLater(() -> {
                    try {
                        addTask((Task) evt.getNewValue());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                break;
            case Server.TASK_SERVING_EVENT:
                Platform.runLater(() -> {
                    try {
                        resetFirstTask((Task) evt.getNewValue());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                break;
        }
    }

    private void addTask(Task task) throws IOException {
        FXMLLoader queuePaneLoader = new FXMLLoader(getClass().getResource(
                "/task_rectangle.fxml"));
        AnchorPane taskPane = queuePaneLoader.load();
        TaskDisplayController taskDisplayController = queuePaneLoader.getController();
        taskDisplayController.initTask(task);
        taskDisplayControllers.add(taskDisplayController);
        noTasksLabel.setText(String.valueOf(taskDisplayControllers.size()));
        tasksBox.getChildren().add(taskPane);
    }

    private void resetFirstTask(Task task) throws IOException {
        taskDisplayControllers.get(0).initTask(task);
    }

    private void removeFirstTask() {
        tasksBox.getChildren().remove(0);
        taskDisplayControllers.remove(0);
        noTasksLabel.setText(String.valueOf(taskDisplayControllers.size()));
    }

    private void resetStatistics(ServerStatistics statistics) {
        noExecutedTasksLabel.setText(String.valueOf(statistics.getNoExecutedTasks()));
        avgWaitingTimeLabel.setText(df.format(statistics.getAvgWaitingTime()));
    }
}
