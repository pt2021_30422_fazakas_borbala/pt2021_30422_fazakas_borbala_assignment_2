package com.bori.Controller;

import com.bori.Controller.Data.QueueSelectionPolicy;
import com.bori.Controller.Util.AlertFactory;
import com.bori.Model.Model;
import com.bori.Model.Simulation.IllegalSimulationConfigurationException;
import com.bori.Model.Simulation.LifeCycleState;
import com.bori.Model.Simulation.SimulationConfiguration;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.converter.IntegerStringConverter;

import java.util.regex.Pattern;

public class SimulationSetupController {

    @FXML
    public TextField noQueuesText;
    @FXML
    public TextField minArrivalTimeText;
    @FXML
    public TextField maxArrivalTimeText;
    @FXML
    public TextField minProcessingTimeText;
    @FXML
    public TextField maxProcessingTimeText;
    @FXML
    public TextField noTasksText;
    @FXML
    public TextField simulationLengthText;
    @FXML
    public Button startStopButton;
    @FXML
    public ChoiceBox<QueueSelectionPolicy> queueSelectionPolicyChoiceBox;
    @FXML
    public Slider simulationSpeedSlider;

    private Model model; //effectively final
    private ObjectProperty<LifeCycleState> simulationState = new SimpleObjectProperty<>(null);
    private ChangeListener<LifeCycleState> simulationStateChangeListener = null;

    public void initialize() {
        startStopButton.setDefaultButton(true);
        queueSelectionPolicyChoiceBox.setItems(FXCollections.observableArrayList(QueueSelectionPolicy.values()));
        queueSelectionPolicyChoiceBox.setValue(QueueSelectionPolicy.RANDOM);
        setupNumericInputs();
    }

    public void initModel(Model model) {
        if (this.model != null) {
            throw new IllegalStateException();
        }
        this.model = model;
    }

    public void onStartStopButtonPressed() {
        if (!model.hasRunningSimulation()) {
            SimulationConfiguration configuration;
            try {
                configuration = getSimulationConfiguration();
                model.runSimulation(configuration);
                simulationState = this.model.getObservableSimulationState();
                addStateListener(this.model.getObservableSimulationState());
                onSimulationStateChange(simulationState.get());
            } catch (NumberFormatException e) {
                AlertFactory.showErrorAlert(e);
            } catch (IllegalSimulationConfigurationException e) {
                AlertFactory.showErrorAlert(e);
            }
        } else {
            model.stopSimulation();
        }
    }

    private void addStateListener(ObjectProperty<LifeCycleState> stateObjectProperty) {
        if (simulationStateChangeListener != null) {
            simulationState.removeListener(simulationStateChangeListener);
        } else {
            simulationStateChangeListener = (observableValue, oldState, newState) -> Platform.runLater(() -> onSimulationStateChange(newState));
        }
        simulationState = stateObjectProperty;
        simulationState.addListener(simulationStateChangeListener);
    }

    private void onSimulationStateChange(LifeCycleState newState) {
        if (newState != LifeCycleState.TERMINATED) {
            startStopButton.setText("Stop");
        } else {
            startStopButton.setText("Start");
        }
    }

    private SimulationConfiguration getSimulationConfiguration() throws IllegalSimulationConfigurationException {
        return SimulationConfiguration.createSimulationConfiguration(Integer.parseInt(simulationLengthText.getText()),
                Integer.parseInt(minProcessingTimeText.getText()),
                Integer.parseInt(maxProcessingTimeText.getText()),
                Integer.parseInt(minArrivalTimeText.getText()),
                Integer.parseInt(maxArrivalTimeText.getText()),
                Integer.parseInt(noQueuesText.getText()),
                Integer.parseInt(noTasksText.getText()),
                queueSelectionPolicyChoiceBox.getValue(),
                (int) simulationSpeedSlider.getValue());
    }

    private void setupNumericInputs() {
        simulationLengthText.setTextFormatter(getIntegerFormatter());
        noQueuesText.setTextFormatter(getIntegerFormatter());
        noTasksText.setTextFormatter(getIntegerFormatter());
        minArrivalTimeText.setTextFormatter(getIntegerFormatter());
        maxArrivalTimeText.setTextFormatter(getIntegerFormatter());
        minProcessingTimeText.setTextFormatter(getIntegerFormatter());
        maxProcessingTimeText.setTextFormatter(getIntegerFormatter());
    }

    private TextFormatter<Integer> getIntegerFormatter() {
        return new TextFormatter<>(
                new IntegerStringConverter(),
                1,
                c -> Pattern.matches("\\d*", c.getText()) ? c : null);
    }
}
