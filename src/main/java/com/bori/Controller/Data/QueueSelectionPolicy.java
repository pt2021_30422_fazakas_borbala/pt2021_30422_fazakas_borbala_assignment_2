package com.bori.Controller.Data;

import com.bori.Model.QueueSelectionStrategies.*;

public enum QueueSelectionPolicy {
    RANDOM(new RandomStrategy(), "Random"),
    LEAST_TASKS(new LeastTasksStrategy(), "Least Tasks in Queue"),
    SHORTEST_WAITING_TIME(new ShortestWaitingTimeStrategy(), "Shortest Waiting Time"),
    SHORTEST_PREV_AVG_WAITING_TIME(new ShortestPrevAvgWaitingTimeStrategy(), "Shortest Avg " +
            "Waiting Time for Previous Tasks");

    private final QueueSelectionStrategy strategy;
    private final String displayedName;

    QueueSelectionPolicy(QueueSelectionStrategy strategy, String displayedName) {
        this.strategy = strategy;
        this.displayedName = displayedName;
    }

    public QueueSelectionStrategy getStrategy() {
        return strategy;
    }

    @Override
    public String toString() {
        return displayedName;
    }
}
