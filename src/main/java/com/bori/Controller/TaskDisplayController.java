package com.bori.Controller;

import com.bori.Controller.Util.TaskColor;
import com.bori.Model.Task.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

public class TaskDisplayController {
    @FXML
    public Label taskIdLabel;
    @FXML
    public Label taskStatisticsLabel;
    @FXML
    public Rectangle taskRectangle;

    @FXML
    public void initialize() {
        taskRectangle.setFill(Paint.valueOf(TaskColor.getRandomColor().getValue()));
    }

    public void initTask(Task task) {
        taskStatisticsLabel.setText(String.format("(%d, %d)",
                                        task.getArrivalTime(),
                                        task.getRemainingServiceTime()));
        taskIdLabel.setText("Task " + task.getId());
    }
}
