package com.bori.Controller;

import com.bori.Model.Model;
import com.bori.Model.Server.Server;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

public class SimulationDisplayController {
    @FXML
    public VBox queueBox;
    @FXML
    public ScrollPane queueScrollPane;

    private Model model; // effectively final

    public void initialize() {
        VBox.setVgrow(queueScrollPane, Priority.ALWAYS);
        queueScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        queueScrollPane.setFitToWidth(true);
    }

    public void initModel(Model model) {
        queueScrollPane.setVisible(false);
        if (this.model != null) {
            throw new IllegalStateException();
        }
        this.model = model;
        this.model.getObservableSimulation().addListener((observableValue, oldSimulation,
                                                          newSimulation) -> {
            try {
                setupQueuePanes();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void setupQueuePanes() throws IOException {
        queueScrollPane.setVisible(true);
        queueBox.getChildren().clear();
        List<Server> simulationServers = model.getSimulationServers();
        simulationServers.sort(Comparator.comparingInt(Server::getId));
        for (Server server : simulationServers) {
            FXMLLoader queuePaneLoader = new FXMLLoader(getClass().getResource(
                    "/queue_pane.fxml"));
            AnchorPane queuePane = queuePaneLoader.load();
            QueueDisplayController queueDisplayController = queuePaneLoader.getController();
            queueDisplayController.initServer(server);
            queueBox.getChildren().add(queuePane);
        }
    }
}
