package com.bori.Controller;

import com.bori.Model.Model;
import com.bori.Model.Simulation.Simulation;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;

public class SimulationStatisticsController implements PropertyChangeListener {
    @FXML
    public Label currentTimeLabel;
    @FXML
    public Label avgWaitingTimeLabel;
    @FXML
    public Label avgServiceTimeLabel;
    @FXML
    public Label peakTimeLabel;
    @FXML
    public Label noExecutedTasksLabel;

    private Model model; // effectively final

    private static final String UNKNOWN_VALUE_SYMBOL = "?";
    private static final String MISSING_VALUE_SYMBOL = "-";
    private static final DecimalFormat df = new DecimalFormat("###.###");

    public void initModel(Model model) {
        if (this.model != null) {
            throw new IllegalStateException();
        }
        this.model = model;
        this.model.getObservableSimulation().addListener((observableValue, oldSimulation,
                                                          newSimulation) -> {
            reset();
            setupStatisticsListener();
            setupTimerListener();
        });
    }

    private void reset() {
        avgServiceTimeLabel.setText(UNKNOWN_VALUE_SYMBOL);
        avgWaitingTimeLabel.setText(UNKNOWN_VALUE_SYMBOL);
        peakTimeLabel.setText(UNKNOWN_VALUE_SYMBOL);
        currentTimeLabel.setText(MISSING_VALUE_SYMBOL);
    }

    private void setupStatisticsListener() {
        this.model.getObservableSimulationStatistics().addListener((observableValue, oldStatistics, currentStatistics) -> Platform.runLater(() -> {
            avgServiceTimeLabel.setText(df.format(currentStatistics.getAvgServiceTime()));
            avgWaitingTimeLabel.setText((df.format(currentStatistics.getAvgWaitingTime())));
            if (currentStatistics.getPeakTime() >= 0) {
                peakTimeLabel.setText(String.valueOf(currentStatistics.getPeakTime()));
            } else {
                peakTimeLabel.setText(MISSING_VALUE_SYMBOL);
            }
            noExecutedTasksLabel.setText(String.valueOf(currentStatistics.getNoExecutedTasks()));
        }));
    }

    private void setupTimerListener() {
        this.model.addCurrentTimeListener(this);
    }

    private void onTimeChangeEvent(int currentTime) {
        currentTimeLabel.setText(String.valueOf(currentTime));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(Simulation.TIME_CHANGE_EVENT_NAME)) {
            Platform.runLater(() -> onTimeChangeEvent((int) evt.getNewValue()));
        }
    }
}
