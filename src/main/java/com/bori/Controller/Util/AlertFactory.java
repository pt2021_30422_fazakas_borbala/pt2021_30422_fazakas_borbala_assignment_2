package com.bori.Controller.Util;

import com.bori.Model.Simulation.IllegalSimulationConfigurationException;
import javafx.scene.control.Alert;

public class AlertFactory {
    public static void showErrorAlert(NumberFormatException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Number");
        alert.setHeaderText("Seems like one of the input numbers is incorrect or empty. Please " +
                "try again!");
        alert.setContentText(e.getMessage());
        alert.show();
    }

    public static void showErrorAlert(IllegalSimulationConfigurationException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Simulation Configuration");
        alert.setHeaderText("The configuration you provided for the simulation is invalid.");
        alert.setContentText(e.getMessage());
        alert.show();
    }
}

