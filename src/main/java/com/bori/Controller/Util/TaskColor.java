package com.bori.Controller.Util;

public enum TaskColor {
    C1("#edd3ab"),
    C2("#e3af94"),
    C3("#dbb07b"),
    C4("#fae196");

    private final String value;

    TaskColor(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static TaskColor getRandomColor() {
        int ind = (int) (Math.random() * TaskColor.values().length);
        return TaskColor.values()[ind];
    }
}
