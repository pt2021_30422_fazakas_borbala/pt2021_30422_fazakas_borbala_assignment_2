package com.bori;

import com.bori.Controller.SimulationDisplayController;
import com.bori.Controller.SimulationSetupController;
import com.bori.Controller.SimulationStatisticsController;
import com.bori.Model.Model;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main extends Application {

    private Model model;
    private BorderPane root;
    private Scene scene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        model = new Model();
        loadRoot();
        loadSetupPane();
        loadStatisticsPane();
        loadSimulationPane();

        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setTitle("Queue Simulator");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void  loadRoot() throws IOException {
        FXMLLoader mainStageLoader = new FXMLLoader(getClass().getResource("/main_stage.fxml"));
        root = mainStageLoader.load();
        root.setPadding(new Insets(10, 10, 10, 10));
        scene = new Scene(root);
    }

    private void loadSetupPane() throws IOException {
        FXMLLoader setupPaneLoader = new FXMLLoader(getClass().getResource("/setup_pane.fxml"));
        AnchorPane setupPane = setupPaneLoader.load();
        SimulationSetupController setupController = setupPaneLoader.getController();
        setupController.initModel(model);
        BorderPane.setAlignment(setupPane, Pos.CENTER);
        root.setTop(setupPane);
    }

    private void loadStatisticsPane() throws IOException {
        FXMLLoader statisticsPaneLoader = new FXMLLoader(getClass().getResource("/statistics_pane" +
                ".fxml"));
        Pane statisticsPane = statisticsPaneLoader.load();
        SimulationStatisticsController statisticsController = statisticsPaneLoader.getController();
        statisticsController.initModel(model);
        root.setRight(statisticsPane);
    }

    private void loadSimulationPane() throws IOException {
        FXMLLoader simulationDisplayLoader = new FXMLLoader(getClass().getResource(
                "/simulation_pane.fxml"));
        AnchorPane simulationPane = simulationDisplayLoader.load();
        SimulationDisplayController simulationDisplayController = simulationDisplayLoader.getController();
        simulationDisplayController.initModel(model);
        root.setCenter(simulationPane);
    }
}
