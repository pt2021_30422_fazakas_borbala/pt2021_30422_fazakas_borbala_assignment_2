package com.bori.Model.Simulation;

import com.bori.Controller.Data.QueueSelectionPolicy;
import com.bori.Model.Model;

public class SimulationConfiguration {
    private final int simulationLength;
    private final int maxServiceTime;
    private final int minServiceTime;
    private final int minArrivalTime;
    private final int maxArrivalTime;
    private final int noServers;
    private final int noTasks;
    private final QueueSelectionPolicy queueSelectionPolicy;
    private final SimulationSpeed simulationSpeed;

    private SimulationConfiguration(int simulationLength, int minServiceTime, int maxServiceTime,
                                    int minArrivalTime, int maxArrivalTime, int noServers,
                                    int noTasks, QueueSelectionPolicy queueSelectionPolicy,
                                    int simulationSpeedId) {
        this.simulationLength = simulationLength;
        this.maxServiceTime = maxServiceTime;
        this.minServiceTime = minServiceTime;
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.noServers = noServers;
        this.noTasks = noTasks;
        this.queueSelectionPolicy = queueSelectionPolicy;
        this.simulationSpeed = SimulationSpeed.getSimulationSpeed(simulationSpeedId);
    }

    public static SimulationConfiguration createSimulationConfiguration(
            int simulationLength, int minServiceTime, int maxServiceTime,
            int minArrivalTime, int maxArrivalTime, int noServers,
            int noTasks, QueueSelectionPolicy queueSelectionPolicy,
            int simulationSpeedId) throws IllegalSimulationConfigurationException {
        if (minServiceTime > maxServiceTime) {
            throw new IllegalSimulationConfigurationException("The maximum service time must be " +
                    "larger than the minimum service time");
        }
        if (minArrivalTime > maxArrivalTime) {
            throw new IllegalSimulationConfigurationException("The maximum arrival time must be " +
                    "larger than the minimum arrival time");
        }
        if (simulationLength <= 0) {
            throw new IllegalSimulationConfigurationException("The simulation length must be at " +
                    "least 1");
        }
        if (minArrivalTime <= 0) {
            throw new IllegalSimulationConfigurationException("The minimum arrival time must be " +
                    "at least 1");
        }
        if (minServiceTime <= 0) {
            throw new IllegalSimulationConfigurationException("The minimum service time must be " +
                    "at least 1");
        }
        if (noServers <= 0) {
            throw new IllegalSimulationConfigurationException("There must be at least 1 queue");
        }
        if (noTasks <= 0) {
            throw new IllegalSimulationConfigurationException("There must be at least 1 task");
        }
        if (noTasks > Model.MAX_NO_TASKS) {
            throw new IllegalSimulationConfigurationException("The maximum number of tasks is " + Model.MAX_NO_TASKS);
        }
        if (noServers > Model.MAX_NO_QUEUES) {
            throw new IllegalSimulationConfigurationException("The maximum number of queues is " + Model.MAX_NO_QUEUES);
        }
        if (simulationLength > Model.MAX_SIMULATION_TIME) {
            throw new IllegalSimulationConfigurationException("The maximum simulation length is " + Model.MAX_SIMULATION_TIME);
        }
        return new SimulationConfiguration(simulationLength, minServiceTime, maxServiceTime,
                minArrivalTime, maxArrivalTime, noServers,
                noTasks, queueSelectionPolicy, simulationSpeedId);
    }

    public int getSimulationLength() {
        return simulationLength;
    }

    public int getMaxServiceTime() {
        return maxServiceTime;
    }

    public int getMinServiceTime() {
        return minServiceTime;
    }

    public int getMinArrivalTime() {
        return minArrivalTime;
    }

    public int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public int getNoServers() {
        return noServers;
    }

    public int getNoTasks() {
        return noTasks;
    }

    public QueueSelectionPolicy getQueueSelectionPolicy() {
        return queueSelectionPolicy;
    }

    public SimulationSpeed getSimulationSpeed() {
        return simulationSpeed;
    }
}
