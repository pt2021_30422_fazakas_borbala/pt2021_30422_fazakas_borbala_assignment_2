package com.bori.Model.Simulation;

import com.bori.Controller.Data.QueueSelectionPolicy;
import com.bori.Model.Model;
import com.bori.Model.QueueSelectionStrategies.QueueSelectionStrategy;
import com.bori.Model.Server.Server;
import com.bori.Model.Task.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CyclicBarrier;

public class TaskScheduler {
    private final Map<Thread, Server> threadToServers;
    private final QueueSelectionStrategy strategy;

    public TaskScheduler(int noServers, CyclicBarrier timerBarrier, QueueSelectionPolicy queueSelectionPolicy,
                         Simulation simulation) {
        List<Server> servers = generateServers(noServers, timerBarrier, simulation);
        this.strategy = queueSelectionPolicy.getStrategy();
        this.threadToServers = new HashMap<>();
        for (Server server : servers) {
            Thread thread = new Thread(server);
            thread.start();
            threadToServers.put(thread, server);
        }
    }

    public void dispatchTask(Task t) {
        strategy.addTask(threadToServers.values(), t);
    }

    public List<Server> getServers() {
        return new ArrayList<>(threadToServers.values());
    }

    public List<Thread> getThreads() {
        return new ArrayList<>(threadToServers.keySet());
    }

    private List<Server> generateServers(int noServers, CyclicBarrier timerBarrier, Simulation simulation) {
        List<Server> result = new ArrayList<>();
        for (int i = 1; i <= noServers; i++) {
            result.add(new Server(timerBarrier, i, Model.MAX_NO_TASKS, simulation));
        }
        return result;
    }
}
