package com.bori.Model.Simulation;

public enum SimulationSpeed {
    VERY_SLOW(5000),
    SLOW(2000),
    MEDIUM(1000),
    FAST(500),
    VERY_FAST(100);

    private final int simulationStepDuration; // in ms

    SimulationSpeed(int simulationStepDuration) {
        this.simulationStepDuration = simulationStepDuration;
    }

    public int getSimulationStepDuration() {
        return simulationStepDuration;
    }

    public static SimulationSpeed getSimulationSpeed(int value) {
        return SimulationSpeed.values()[value - 1];
    }
}
