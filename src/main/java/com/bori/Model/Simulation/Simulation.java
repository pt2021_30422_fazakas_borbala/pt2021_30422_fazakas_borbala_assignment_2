package com.bori.Model.Simulation;

import com.bori.Model.Server.Server;
import com.bori.Model.Task.Task;
import com.bori.Model.Task.TaskFactory;
import com.bori.Model.Util.Logger.SimulationLogger;
import com.bori.Model.Util.PropertyChangeObservable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import org.jetbrains.annotations.NotNull;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Simulation implements Runnable, PropertyChangeObservable {
    // simulation parameters
    private final SimulationConfiguration config;

    // simulation state
    private final ObjectProperty<LifeCycleState> state;
    private final List<Task> tasks; //sorted by arrivalTime
    private final AtomicInteger currentTime;

    // simulation statistics
    private final AtomicInteger currNoTasks;
    private final Lock statisticsUpdateLock;
    private final ObjectProperty<SimulationStatistics> simulationStatisticsProperty;

    // utils
    private final TaskScheduler taskScheduler;
    private final CyclicBarrier timerBarrier;

    // for PropertyChangeObservable
    protected PropertyChangeSupport support = new PropertyChangeSupport(this);

    // constants
    private final int SIMULATION_TIME_UNIT;
    public final static String TIME_CHANGE_EVENT_NAME = "TimeChange";


    public Simulation(@NotNull SimulationConfiguration config) {
        this.state = new SimpleObjectProperty<>(LifeCycleState.PREPARED);
        this.currentTime = new AtomicInteger(0); // starts from 1
        this.config = config;
        this.tasks = generateRandomTasks(config);
        this.statisticsUpdateLock = new ReentrantLock();
        this.timerBarrier = new CyclicBarrier(config.getNoServers() + 1, this::onNewSimulationStep);
        this.taskScheduler = new TaskScheduler(config.getNoServers(), timerBarrier,
                config.getQueueSelectionPolicy(), this);
        this.currNoTasks = new AtomicInteger(0);
        this.simulationStatisticsProperty =
                new SimpleObjectProperty<>(new SimulationStatistics(taskScheduler.getServers(),
                        -1, 0));
        this.SIMULATION_TIME_UNIT = config.getSimulationSpeed().getSimulationStepDuration();
    }

    /**
     * The simulation runs from currentTimeProperty = 1 to currentTimeProperty = simulationLength.
     * At time=0 the setup is done.
     */
    @Override
    public void run() {
        SimulationLogger.logSimulationStart(config);
        state.setValue(currentTime.get() < config.getSimulationLength() ?
                LifeCycleState.RUNNING :
                LifeCycleState.TERMINATING);
        try {
            timerBarrier.await();
            while (state.get() == LifeCycleState.RUNNING) {
                scheduleTasks();
                Thread.sleep(SIMULATION_TIME_UNIT);
                timerBarrier.await();
            }
        } catch (InterruptedException | BrokenBarrierException e) {
            state.set(LifeCycleState.TERMINATING);
            stopAllServers();
            timerBarrier.reset();
        }
        joinAllServers();
        updateStatistics();
        SimulationLogger.logSimulationStatistics(simulationStatisticsProperty.get());
        state.setValue(LifeCycleState.TERMINATED);
    }

    public int getCurrentTime() {
        return currentTime.get();
    }

    public void incrementNoTasks() {
        statisticsUpdateLock.lock();
        int noTasks = currNoTasks.incrementAndGet();
        if (noTasks > simulationStatisticsProperty.get().getMaxNoTasks()) {
            simulationStatisticsProperty.set(new SimulationStatistics(
                                            simulationStatisticsProperty.get().getAvgServiceTime(),
                                            simulationStatisticsProperty.get().getAvgWaitingTime(),
                                            simulationStatisticsProperty.get().getNoExecutedTasks(),
                                            currentTime.get(),
                                            noTasks));
        }
        statisticsUpdateLock.unlock();
    }

    public void decrementNoTasks() {
        currNoTasks.decrementAndGet();
    }

    public LifeCycleState getState() {
        return state.get();
    }

    public ObjectProperty<LifeCycleState> getObservableState() {
        return state;
    }

    public void terminate() {
        state.set(LifeCycleState.TERMINATING);
    }

    public ObjectProperty<SimulationStatistics> getObservableSimulationStatistics() {
        return simulationStatisticsProperty;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    public List<Server> getServers() {
        return taskScheduler.getServers();
    }

    private void onNewSimulationStep() {
        SimulationLogger.logSimulationState(currentTime.get(), tasks, taskScheduler.getServers());
        statisticsUpdateLock.lock();
        updateStatistics();
        currentTime.incrementAndGet();
        statisticsUpdateLock.unlock();
        if (reachedEndOfSimulation()) {  // end of simulation
            state.setValue(LifeCycleState.TERMINATING);
        }
        if (state.get() == LifeCycleState.TERMINATING) {
            stopAllServers();
        } else {
            support.firePropertyChange(TIME_CHANGE_EVENT_NAME, -1, currentTime.get());
        }
    }

    private boolean reachedEndOfSimulation() {
        return currentTime.get() == config.getSimulationLength() + 1 ||
                simulationStatisticsProperty.get().getNoExecutedTasks() == config.getNoTasks();
    }

    private void updateStatistics() {
        simulationStatisticsProperty.set(new SimulationStatistics(taskScheduler.getServers(),
                simulationStatisticsProperty.get().getPeakTime(),
                simulationStatisticsProperty.get().getMaxNoTasks()));
    }

    private void joinAllServers() {
        List<Thread> threads = taskScheduler.getThreads();
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void stopAllServers() {
        Collection<Server> servers = taskScheduler.getServers();
        for (Server server : servers) {
            server.terminate();
        }
    }

    private void scheduleTasks() {
        while (!tasks.isEmpty() && tasks.get(0).getArrivalTime() == currentTime.get()) {
            taskScheduler.dispatchTask(tasks.get(0));
            tasks.remove(0);
        }
    }

    private List<Task> generateRandomTasks(SimulationConfiguration config) {
        List<Task> randomTasks = new ArrayList<>();
        for (int i = 1; i <= config.getNoTasks(); i++) {
            randomTasks.add(TaskFactory.generateRandomTask(i, config.getMinArrivalTime(), config.getMaxArrivalTime(),
                    config.getMinServiceTime(), config.getMaxServiceTime()));
        }
        randomTasks.sort(Comparator.comparingInt(Task::getArrivalTime));
        return randomTasks;
    }
}