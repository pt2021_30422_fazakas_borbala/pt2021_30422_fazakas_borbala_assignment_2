package com.bori.Model.Simulation;

import com.bori.Model.Server.Server;

import java.util.Collection;

public class SimulationStatistics {
    private final double avgWaitingTime;
    private final double avgServiceTime;
    private final int peakHour;
    private final int noExecutedTasks;
    private final int maxNoTasks;

    public SimulationStatistics(Collection<Server> servers, int peakHour, int maxNoTasks) {
        this.peakHour = peakHour;
        this.maxNoTasks = maxNoTasks;
        this.noExecutedTasks = servers.stream().mapToInt(Server::getNoExecutedTasks).sum();
        if (noExecutedTasks == 0) {
            this.avgWaitingTime = 0;
            this.avgServiceTime = 0;
        } else {
            this.avgWaitingTime =
                    (double) servers.stream().mapToInt(Server::getTotalWaitingTime).sum() / noExecutedTasks;
            this.avgServiceTime =
                    (double) servers.stream().mapToInt(Server::getTotalServiceTime).sum() / noExecutedTasks;
        }
    }

    public SimulationStatistics(double avgServiceTime, double avgWaitingTime, int noExecutedTasks,
                                int peakHour, int maxNoTasks) {
        this.peakHour = peakHour;
        this.maxNoTasks = maxNoTasks;
        this.noExecutedTasks = noExecutedTasks;
        this.avgServiceTime = avgServiceTime;
        this.avgWaitingTime = avgWaitingTime;
    }

    public double getAvgWaitingTime() {
        return avgWaitingTime;
    }

    public double getAvgServiceTime() {
        return avgServiceTime;
    }

    public int getPeakTime() {
        return peakHour;
    }

    public int getNoExecutedTasks() {
        return noExecutedTasks;
    }

    public int getMaxNoTasks() {
        return maxNoTasks;
    }
}
