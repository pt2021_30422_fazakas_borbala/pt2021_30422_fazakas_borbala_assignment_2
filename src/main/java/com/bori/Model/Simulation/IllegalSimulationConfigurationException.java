package com.bori.Model.Simulation;

public class IllegalSimulationConfigurationException extends Exception {
    IllegalSimulationConfigurationException(String message) {
        super(message);
    }
}
