package com.bori.Model.Simulation;

public enum LifeCycleState {
    PREPARED,
    RUNNING,
    TERMINATING,
    TERMINATED
}
