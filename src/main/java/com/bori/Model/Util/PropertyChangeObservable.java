package com.bori.Model.Util;

import java.beans.PropertyChangeListener;

public interface PropertyChangeObservable {

    void addPropertyChangeListener(PropertyChangeListener listener);

    void removePropertyChangeListener(PropertyChangeListener listener);

}
