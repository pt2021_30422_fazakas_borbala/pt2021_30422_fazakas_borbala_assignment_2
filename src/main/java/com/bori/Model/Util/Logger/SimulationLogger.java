package com.bori.Model.Util.Logger;

import com.bori.Model.Server.Server;
import com.bori.Model.Simulation.SimulationConfiguration;
import com.bori.Model.Simulation.SimulationStatistics;
import com.bori.Model.Task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SimulationLogger {
    public static Logger LOGGER = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    private static final DecimalFormat df = new DecimalFormat("###.###");

    public static void logSimulationStart(SimulationConfiguration configuration) {
        LOGGER.info("----------------------------Simulation Started");
        LOGGER.info("Simulation length: {}", configuration.getSimulationLength());
        LOGGER.info("No. of Queues: {}, No. of Tasks: {}", configuration.getNoServers(),
                configuration.getNoTasks());
        LOGGER.info("Min service time: {}, Max. serive time: {}",
                configuration.getMinServiceTime(), configuration.getMaxServiceTime());
        LOGGER.info("Min arrival time: {}, Max. arrival time: {}",
                configuration.getMinArrivalTime(), configuration.getMaxArrivalTime());
        LOGGER.info("Queue Selection Policy: {}", configuration.getQueueSelectionPolicy());
        LOGGER.info("Speed: {}", configuration.getSimulationSpeed());
    }

    public static void logSimulationState(int currentTime, Collection<Task> waitingTasks,
                                          List<Server> servers) {
        LOGGER.info("-----------------Time {}", currentTime);
        LOGGER.info("---------Waiting tasks: ");
        for (Task task : waitingTasks) {
            LOGGER.info("{}", task);
        }
        LOGGER.info("---------Queues' Status: ");
        servers.sort(Comparator.comparingInt(Server::getId));
        for (Server server : servers) {
            LOGGER.info("---Tasks of {}", server);
            List<Task> tasksOfServer = server.getTasks();
            if (tasksOfServer.isEmpty()) {
                LOGGER.info("Queue is closed");
            }
            for (Task task : tasksOfServer) {
                LOGGER.info("{}", task);
            }
        }
    }

    public static void logSimulationStatistics(SimulationStatistics simulationStatistics) {
        LOGGER.info("-----------------Simulation Statistics");
        LOGGER.info("Average waiting time: {}",
                df.format(simulationStatistics.getAvgWaitingTime()));
        LOGGER.info("Average service time: {}",
                df.format(simulationStatistics.getAvgServiceTime()));
        LOGGER.info("Peak hour: {}", simulationStatistics.getPeakTime());
    }
}
