package com.bori.Model.Server;

public class ServerStatistics {
    private final int noExecutedTasks;
    private final int totalWaitingTime;
    private final int totalServiceTime;

    public ServerStatistics(int noExecutedTasks, int totalWaitingTime, int totalServiceTime) {
        this.noExecutedTasks = noExecutedTasks;
        this.totalWaitingTime = totalWaitingTime;
        this.totalServiceTime = totalServiceTime;
    }

    public int getNoExecutedTasks() {
        return noExecutedTasks;
    }

    public int getTotalWaitingTime() {
        return totalWaitingTime;
    }

    public int getTotalServiceTime() {
        return totalServiceTime;
    }

    public double getAvgWaitingTime() {
        if (noExecutedTasks == 0) {
            return 0;
        }
        return (double) totalWaitingTime / (double) noExecutedTasks;
    }
}
