package com.bori.Model.Server;

import com.bori.Model.Simulation.LifeCycleState;
import com.bori.Model.Simulation.Simulation;
import com.bori.Model.Task.Task;
import com.bori.Model.Util.PropertyChangeObservable;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Consumer
 */
public class Server implements Runnable, PropertyChangeObservable {
    // id and content
    private final int id;
    private final BlockingQueue<Task> tasks;

    // state
    private final CyclicBarrier timerBarrier;
    private volatile LifeCycleState state;
    private final Simulation simulation;

    private final AtomicInteger currentWaitingTime; //sum of service times of tasks in queue
    private final AtomicReference<ServerStatistics> serverStatistics;

    // for observable
    PropertyChangeSupport support = new PropertyChangeSupport(this);
    public static final String NEW_TASK_EVENT = "NewTaskEvent";
    public static final String TASK_SERVING_EVENT = "TaskServingEvent";
    public static final String TASK_FINISHING_EVENT = "TaskFinishingEvent";
    public static final String STATISTICS_CHANGE_EVENT = "StatisticsChangeEvent";

    public Server(CyclicBarrier timerBarrier, int id, int maxNoTasks, Simulation simulation) {
        this.timerBarrier = timerBarrier;
        this.id = id;
        this.state = LifeCycleState.PREPARED;
        this.simulation = simulation;
        tasks = new LinkedBlockingDeque<>(maxNoTasks);
        this.serverStatistics = new AtomicReference<>(new ServerStatistics(0, 0, 0));
        this.currentWaitingTime = new AtomicInteger(0);
    }

    @Override
    public void run() {
        this.state = LifeCycleState.RUNNING;
        int currentTime;
        try {
            timerBarrier.await();
            while (state == LifeCycleState.RUNNING) {
                currentTime = simulation.getCurrentTime(); //constant until next await
                Task servedTask = tasks.peek();
                if (servedTask != null && servedTask.getArrivalTime() < currentTime) {
                    serveTask(servedTask, currentTime);
                }
                timerBarrier.await();
            }
        } catch (InterruptedException | BrokenBarrierException e) {
            terminate();
        }
        state = LifeCycleState.TERMINATED;
    }

    public void addTask(Task newTask) {
        try {
            tasks.put(newTask); //if cannot insert immediately, block until possible
            currentWaitingTime.addAndGet(newTask.getServiceTime());
            simulation.incrementNoTasks();
            support.firePropertyChange(NEW_TASK_EVENT, null, newTask);
        } catch (InterruptedException e) {
            terminate();
        }
    }

    /**
     * Doesn't guarantee immediate termination.
     */
    public void terminate() {
        state = LifeCycleState.TERMINATING;
    }

    public int getId() {
        return this.id;
    }

    public int getNoTasks() {
        return tasks.size();
    }

    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    public int getWaitingTime() {
        return currentWaitingTime.get();
    }

    public int getTotalWaitingTime() {
        return serverStatistics.get().getTotalWaitingTime();
    }

    public int getNoExecutedTasks() {
        return serverStatistics.get().getNoExecutedTasks();
    }

    public int getTotalServiceTime() {
        return serverStatistics.get().getTotalServiceTime();
    }

    public double getAvgWaitingTime() {
        return serverStatistics.get().getAvgWaitingTime();
    }

    @Override
    public String toString() {
        return "Queue " + id;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        support.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        support.removePropertyChangeListener(listener);
    }

    private void serveTask(Task servedTask, int currentTime) throws InterruptedException {
        servedTask.serve(currentTime);
        currentWaitingTime.decrementAndGet();
        support.firePropertyChange(TASK_SERVING_EVENT, null, servedTask);
        if (servedTask.isFinished()) {
            handleFinishedTask(servedTask);
        }
    }

    private void handleFinishedTask(Task finishedTask) throws InterruptedException {
        serverStatistics.set(new ServerStatistics(serverStatistics.get().getNoExecutedTasks() + 1,
                serverStatistics.get().getTotalWaitingTime() + finishedTask.getWaitingTime(),
                serverStatistics.get().getTotalServiceTime() + finishedTask.getServiceTime()));
        tasks.take(); //it's guaranteed that finishedTask is at the front of the queue at this point
        support.firePropertyChange(TASK_FINISHING_EVENT, null, getTasks());
        support.firePropertyChange(STATISTICS_CHANGE_EVENT, null, serverStatistics.get());
        simulation.decrementNoTasks();
    }
}
