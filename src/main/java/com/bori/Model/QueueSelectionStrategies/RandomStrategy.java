package com.bori.Model.QueueSelectionStrategies;

import com.bori.Model.Server.Server;
import com.bori.Model.Task.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class RandomStrategy implements QueueSelectionStrategy {

    @Override
    public void addTask(Collection<Server> servers, Task t) {
        if (servers.isEmpty()) {
            throw new IllegalArgumentException("There must be at least one available server");
        }
        Random random = new Random();
        int queueIndex = random.nextInt(servers.size());
        new ArrayList<>(servers).get(queueIndex).addTask(t);
    }
}
