package com.bori.Model.QueueSelectionStrategies;

import com.bori.Model.Server.Server;
import com.bori.Model.Task.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;

public class LeastTasksStrategy implements QueueSelectionStrategy {
    @Override
    public void addTask(Collection<Server> servers, Task t) {
        Optional<Server> consumer =
                servers.stream().min(Comparator.comparingInt(Server::getNoTasks));
        if (consumer.isEmpty()) {
            throw new IllegalArgumentException("There must be at least one available server");
        }
        consumer.get().addTask(t);
    }
}
