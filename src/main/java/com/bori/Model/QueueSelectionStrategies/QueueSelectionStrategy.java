package com.bori.Model.QueueSelectionStrategies;

import com.bori.Model.Server.Server;
import com.bori.Model.Task.Task;

import java.util.Collection;

public interface QueueSelectionStrategy {
    /**
     * Requirement: servers.size() > 0
     */
    void addTask(Collection<Server> servers, Task t);
}
