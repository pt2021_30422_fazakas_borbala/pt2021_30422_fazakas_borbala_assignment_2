package com.bori.Model.Task;

import java.util.concurrent.atomic.AtomicReference;

public class Task {
    private final int arrivalTime;
    private final int serviceTime;
    private final int id;
    private final AtomicReference<TaskStatistics> taskStatistics;

    public Task(int id, int arrivalTime, int serviceTime) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
        this.taskStatistics = new AtomicReference<>(new TaskStatistics(this.serviceTime));
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int getId() {
        return id;
    }

    public int getRemainingServiceTime() {
        return taskStatistics.get().getRemainingServiceTime();
    }

    public boolean isFinished() {
        return taskStatistics.get().isFinished();
    }

    public int getWaitingTime() {
        return taskStatistics.get().getWaitingTime(arrivalTime);
    }

    public void serve(int currentTime) {
        if (this.taskStatistics.get().isStarted()) {
            this.taskStatistics.set(new TaskStatistics(this.taskStatistics.get().getRemainingServiceTime() - 1,
                    this.taskStatistics.get().getServiceTimeStart()));
        } else {
            this.taskStatistics.set(new TaskStatistics(this.taskStatistics.get().getRemainingServiceTime() - 1,
                    currentTime));
        }
    }

    @Override
    public String toString() {
        return "Task(" + id + ", " + arrivalTime + ", " + taskStatistics.get().getRemainingServiceTime() + ")";
    }
}
