package com.bori.Model.Task;

public class TaskStatistics {
    private final int remainingServiceTime;
    private final int serviceTimeStart; //a negative number means it is not yet started

    public TaskStatistics(int remainingServiceTime, int serviceTimeStart) {
        this.remainingServiceTime = remainingServiceTime;
        this.serviceTimeStart = serviceTimeStart;
    }

    public TaskStatistics(int remainingServiceTime) {
        this(remainingServiceTime, -1);
    }

    public int getRemainingServiceTime() {
        return remainingServiceTime;
    }

    public int getServiceTimeStart() {
        return serviceTimeStart;
    }

    public boolean isFinished() {
        return remainingServiceTime <= 0;
    }

    public boolean isStarted() {
        return serviceTimeStart >= 0;
    }

    public int getWaitingTime(int arrivalTime) {
        if (serviceTimeStart < 0) {
            throw new UnsupportedOperationException("This task is still waiting");
        }
        return serviceTimeStart - arrivalTime;
    }
}
