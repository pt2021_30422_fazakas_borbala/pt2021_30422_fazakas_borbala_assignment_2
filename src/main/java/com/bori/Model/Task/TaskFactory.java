package com.bori.Model.Task;


import java.util.Random;

public class TaskFactory {
    static Random randomGenerator = new Random();

    public static Task generateRandomTask(int taskId, int minArrivalTime, int maxArrivalTime,
                                          int minServiceTime, int maxServiceTime) {
        int arrivalTime = getIntInRange(minArrivalTime, maxArrivalTime);
        int serviceTime = getIntInRange(minServiceTime, maxServiceTime);
        return new Task(taskId, arrivalTime, serviceTime);
    }

    /**
     * Inclusive min and max
     */
    private static int getIntInRange(int min, int max) {
        int bound = max - min + 1;
        return randomGenerator.nextInt(bound) + min;
    }
}
