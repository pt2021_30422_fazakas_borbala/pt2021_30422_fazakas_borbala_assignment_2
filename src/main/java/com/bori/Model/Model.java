package com.bori.Model;

import com.bori.Model.Server.Server;
import com.bori.Model.Simulation.LifeCycleState;
import com.bori.Model.Simulation.Simulation;
import com.bori.Model.Simulation.SimulationConfiguration;
import com.bori.Model.Simulation.SimulationStatistics;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.beans.PropertyChangeListener;
import java.util.List;

public class Model {
    public static final int MAX_NO_QUEUES = 50;
    public static final int MAX_NO_TASKS = 2000;
    public static final int MAX_SIMULATION_TIME = 3600;

    private final ObjectProperty<Simulation> simulationProperty = new SimpleObjectProperty<>();

    public void runSimulation(SimulationConfiguration configuration) {
        simulationProperty.set(new Simulation(configuration));
        Thread simulationThread = new Thread(simulationProperty.get());
        simulationThread.start();
    }

    public boolean hasRunningSimulation() {
        if (simulationProperty.get() == null) {
            return false;
        }
        return simulationProperty.get().getState() != LifeCycleState.TERMINATED;
    }

    public void stopSimulation() {
        if (simulationProperty.get() == null) {
            throw new IllegalStateException("No running simulation");
        }
        simulationProperty.get().terminate();
    }

    public ObjectProperty<LifeCycleState> getObservableSimulationState() {
        if (simulationProperty.get() != null) {
            return simulationProperty.get().getObservableState();
        } else {
            throw new IllegalStateException("No simulation");
        }
    }

    public ObjectProperty<SimulationStatistics> getObservableSimulationStatistics() {
        if (simulationProperty.get() != null) {
            return simulationProperty.get().getObservableSimulationStatistics();
        } else {
            throw new IllegalStateException("No simulation");
        }
    }

    public ObjectProperty<Simulation> getObservableSimulation() {
        return simulationProperty;
    }

    public void addCurrentTimeListener(PropertyChangeListener listener) {
        if (simulationProperty.get() != null) {
            simulationProperty.get().addPropertyChangeListener(listener);
        } else {
            throw new IllegalStateException("No simulation");
        }
    }

    public void removeCurrentTimeListener(PropertyChangeListener listener) {
        if (simulationProperty.get() != null) {
            simulationProperty.get().removePropertyChangeListener(listener);
        } else {
            throw new IllegalStateException("No simulation");
        }
    }

    public List<Server> getSimulationServers() {
        if (simulationProperty.get() != null) {
            return simulationProperty.get().getServers();
        } else {
            throw new IllegalStateException("No simulation");
        }
    }
}
